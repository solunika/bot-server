var app = require('../app');
var debug = require('debug')('primeDiceServer:server');
var http = require('http');
/**
 * Listen on provided port, on all network interfaces.
 */

//  Set the environment variables we need.
var ipaddress = process.env.OPENSHIFT_NODEJS_IP;
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

if (typeof ipaddress === "undefined") {
    debug.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
    ipaddress = "127.0.0.1";
};

app.listen(port, ipaddress, onListening);

function onListening() {
    debug('Listening on ' + ipaddress + ':' + port);
}