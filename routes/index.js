var express = require('express');
var router = express.Router();
var util = require('util')

var primeDiceBot = require('../bot/primeDiceBot.js');
var passWord = 'puredezapallo';

function validatePassword(callback) {
    return function(req, res, next) {
        if (req.query&&req.query.p===passWord) {
            callback(req, res, next);
        } else {
            res.send("Successful pleas wait...");
        }
    }
}

router.get('/', function(req, res, next) {
    res.send({hola:'hola'});
});
router.get('/server_status', validatePassword(function(req, res, next) {
    res.send(primeDiceBot.status());  
}));

router.get('/data', validatePassword(function(req, res, next) {
    res.send(primeDiceBot.data());
}));

router.get('/exchange_rate', validatePassword(function(req, res, next) {
   primeDiceBot.updateExchangeRate(function(data){
       res.send(data);
   });
}));
router.post('/initialize', validatePassword(function(req, res, next) {
    req.checkBody('safeStreak').notEmpty().isInt();
    req.checkBody('token').notEmpty();
    req.checkBody('waitForSafeStreak').notEmpty().isInt();
    
    var errors = req.validationErrors();
    if (errors) {
        res.send('There have been validation errors: ' + util.inspect(errors), 400);
    } else {
        primeDiceBot.initialize(req.body, function(err, data){
            if (err) next(err);
            else res.send(data);
        })
    }
}));

router.post('/start', validatePassword(function(req, res, next) {
    primeDiceBot.start(function(err, serverStatus) {
        if (err) next(err);
        else res.send(serverStatus);
    });
}));
router.post('/stop', validatePassword(function(req, res, next) {
    primeDiceBot.stop('Stop by user', function(err, serverStatus) {
        if (err) next(err);
        else res.send(serverStatus);
    });
}));

module.exports = router;
