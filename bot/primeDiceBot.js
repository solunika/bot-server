var request = require('request');
var async = require('async');
var moment = require('moment');
var debug = require('debug')('primeDiceServer:bot');
var randomstring = require("randomstring");

var safeStreak = null;
var token = null;
var waitForSafeStreak = null;
var actualWaitforSafeStreak = 0;
var withdrawAddress = null;
var withdrawWhen = null;
var changeSeedsEvery = null;

var baseUrl = 'https://api.primedice.com/api/';
var accessTokenParam = '?access_token=';
var betUrl = baseUrl + 'bet'+accessTokenParam;
var userInfoUrl = baseUrl + 'users/1'+accessTokenParam;
var withDrawUrl = baseUrl + 'withdraw'+accessTokenParam;
var changeSeedUrl = baseUrl + 'seed'+accessTokenParam;

var bitCoinExchangeRateUrl = 'https://api.bitcoinaverage.com/exchanges/USD';

var condition = '<';
var targetPercentage = 79.2;

var bets = 0;
var lostInARow =0;
var perdidasAsumidas = 0;
var baseBet = 0;
var dummyBet = 10;
var lastStreak = {};
var safePercentage = 2;
var actualSafeStreak = safeStreak;

var initialBalance = null;

var lastUser = null;
var lastWin = true;
var stopSignal = false;
var usdExchangeRate = 413.03;

var serverStatus = {
    initialized: false,
    started: false,
    stopped: true,
    stopping: false,
    startTimer: null,
    endTimer: null
};

var errorMessage = '';
var data = {};
var forceChangeSeed = false;

function updateExchangeRate(callback) {
    request.get(bitCoinExchangeRateUrl, function(err, data) {
        if (!err) {
            usdExchangeRate = JSON.parse(data.body).coinbase.rates.last;
        }
        if (callback) callback(usdExchangeRate.toString());
    });
}

function updateData(user) {
    data.bets = bets;
    data.betTime = moment();
    data.baseBet = baseBet;
    data.profit = user.balance - initialBalance;
    data.username = user.username;
    data.actualSafeStreak = actualSafeStreak;
    data.probabilityOfStreak = Math.round(probabilityOfStreak(bets, actualSafeStreak, lastStreak[actualSafeStreak.toString()].repeat)*100)/100;
    data.lastStreak = lastStreak;
    // for (var i=safeStreak;i<15;i++) {
    //     var lastStreakInstance = lastStreak[i.toString()];
    //     lastStreakInstance.probabilityOfStreak = probabilityOfStreak(bets, i, lastStreakInstance.repeat);
    // }
    data.balance = user.balance;
    data.usdExchangeRate = usdExchangeRate;
    data.balanceInUsd = user.balance/100/1000000*usdExchangeRate;
    var deltaTime = data.betTime.diff(serverStatus.startTimer, 'seconds');
    data.betsPerMinute = bets / (deltaTime/60);
    debug(data);
}

function getData() {
    return data;
}
function changeSeed(callback) {
    var newSeed = randomstring.generate({
        length: 29,
        charset: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    })
    request.post(changeSeedUrl+token, {form: {seed: newSeed}}, function(err, response){
        if (err||response.body=='Unauthorized') {
            stop("No se pudo cambiar el seed. Puede ser el token: " + err + " "+response.body);
        }
        else {
            getUserInfo(callback);
        }
    });
}

function getUserInfo(callback) {
    request.get(userInfoUrl+token, function(err, response) {
        if (err||response.body=='Unauthorized') {
            stop("No empezo, hubo un error. Puede ser el token: " + err + " "+response.body);
        } else {
            var user = JSON.parse(response.body).user;
            actualSafeStreak = getSafeStreak(bets, safeStreak+actualWaitforSafeStreak);
            baseBet = calcMaxBaseBet(user.balance, actualSafeStreak);
            if (!initialBalance) {
                initialBalance = user.balance;
            }
            updateData(user);
            callback(user);
        }
    });
}

function withdrawBits(amount, callback) {
    request.post(withDrawUrl+token, {form: {amount: amount, address: withdrawAddress}}, function(err, response) {
        if (err||response.body=='Unauthorized') {
            stop("No empezo, hubo un error. Puede ser el token: " + err + " "+data.body);
        } else {
            callback();
        }
    });
}

function realBet(next) {
    var params = {
        form: { amount: baseBet, condition: condition, target: targetPercentage },
        timeout: 4000
    };
    request.post(betUrl+token, params, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var r = JSON.parse(body);
            updateData(r.user);
            if (r.bet && r.bet.win == true) {
                onWinFunction(r.user);
            } else {
                onLoseFunction(r.user);
            }
        } else {
            onErrorFunction(error, response);
        }
        if (getData().profit>=withdrawWhen) {
            forceChangeSeed = true;
            withdrawBits(getData().profit, next);
        } else {
            next();
        }
    })
}

function calcMaxBaseBet(balance, _maxLoseStreak) {
    var divisor = 0;
    balance /= 100;
    for (var i = 0; i < (_maxLoseStreak); i++) {
        divisor += (Math.pow(5, i));
    }
    var maxBaseBet = balance / divisor * 100;
    if (maxBaseBet<1) {
        maxBaseBet = 1;
    } else {
        maxBaseBet = maxBaseBet-1;
    }
    return parseFloat(maxBaseBet);
}

var onWinFunction = function(user) {
    lastWin = true;
    actualSafeStreak = getSafeStreak(bets, safeStreak+actualWaitforSafeStreak);
    baseBet = calcMaxBaseBet(user.balance, actualSafeStreak);
    if (lostInARow>=safeStreak) {
        for (var i=lostInARow;i>=safeStreak;i--){
            lastStreak[i.toString()].bets = bets;
            if (actualWaitforSafeStreak>0) {
                lastStreak[i.toString()].repeat = 0;    
            } else {
                lastStreak[i.toString()].repeat ++;
            }
        }
        actualWaitforSafeStreak = 0;
    }
    lostInARow = 0;
}
var onLoseFunction = function(user) {
    lastWin = false;
    baseBet *= 5;
    lostInARow++;
    if (lostInARow>4) {
        console.log("data: "+JSON.stringify(data)+" lostInARow: "+lostInARow+" baseBet: "+baseBet)
    }
}

var onErrorFunction = function(error, response) {
    if (response&&response.statusCode==401) {
        stop("Token Invalidado");
    } else {
        debug("Error: "+error+" response: "+JSON.stringify(response));
        bets--;
    }
}

function getSafeStreak(_bets, _safeStreak) {
    if (probabilityOfStreak(_bets, _safeStreak, lastStreak[_safeStreak.toString()].repeat)<safePercentage) {
        return _safeStreak;
    } else {
        return getSafeStreak(_bets, _safeStreak+1);
    }
}


function probabilityOfStreak(bets, streak, lastStreakRepeated) {
    var probability = Math.pow(0.208, streak)*100;
    var probabilityBets = probability*bets;
    var repeat = 100*lastStreakRepeated;
    var ret = probabilityBets;
    if (probabilityBets>repeat) {
        if (probabilityBets-repeat<probability) {
            ret = probability;
        } else {
            ret = probabilityBets-repeat;
        }
    }
    return ret;
}

function bet(next) {
    bets++;
    if (lastWin&&(bets>=changeSeedsEvery||forceChangeSeed===true)) {
        forceChangeSeed = false;
        changeSeed(function(user) {
            reset();
            next();
        });
    } else {
        if (stopSignal&&lastWin) {
            next('stop');
        } else {
            actualSafeStreak = getSafeStreak(bets, safeStreak+actualWaitforSafeStreak);
            if (lostInARow>=actualSafeStreak) {
                stop("no more money");
                console.log("data: "+ data);
            } else {
                realBet(next);
            }
        }
    }
}

function setParams(params) {
    safeStreak = parseInt(params.safeStreak);
    token = params.token;
    waitForSafeStreak = parseInt(params.waitForSafeStreak);
    safePercentage = params.safePercentage || 1.9;
    withdrawAddress = params.withdrawAddress;
    withdrawWhen = parseInt(params.withdrawWhen)+20000; // fee
    changeSeedsEvery = params.changeSeedsEvery;
    reset();
}

function initialize(params, callback) {
    if (!serverStatus.started) {
        setParams(params);
        var setInitialize = function(user) {
            serverStatus.initialized = true;
            callback(null, getData());
        }
        if (params.initWithNewSeed) {
            changeSeed(setInitialize);
        } else {
            getUserInfo(setInitialize);
        }
    } else {
        callback("can't initialize if its up");
    }
}

function stop (message, callback) {
    if (serverStatus.started) {
        errorMessage = message;
        serverStatus.stopping = true;
        stopSignal = true;
        if (callback) callback(null, serverStatus);
    } else {
        if (callback) callback("not up");
    }
}
function reset (savedInstance) {
    serverStatus.startTimer = moment();
    bets = 0;
    actualWaitforSafeStreak = waitForSafeStreak;
    for (var i=safeStreak;i<15;i++) {
        lastStreak[i.toString()] = {};
        lastStreak[i.toString()].bets = 0;
        lastStreak[i.toString()].repeat = 0;
        lastStreak[i.toString()].probabilityOfStreak = probabilityOfStreak(0, i, 0);
    }
}

function start (callback) {
    if (serverStatus.initialized) {
        stopSignal = false;
        serverStatus.started = true;
        serverStatus.stopped = false;
        async.forever(bet, function(message) {
            if (message=='stop') { 
                reset();
                serverStatus.endTimer = moment();
                serverStatus.stopped = true;
                serverStatus.started = false;
            }
        })
        callback(null, serverStatus);
    } else {
        callback("not initialized");
    }
}

function getStatus() {
    return serverStatus;
}

updateExchangeRate();

module.exports = {
    start: start,
    stop: stop,
    data: getData,
    status: getStatus,
    initialize: initialize,
    updateExchangeRate: updateExchangeRate
}